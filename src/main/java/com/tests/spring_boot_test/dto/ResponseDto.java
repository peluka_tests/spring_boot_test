package com.tests.spring_boot_test.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public class ResponseDto {

    private boolean valid;
    private List<String> sugestedUsernames;
}
