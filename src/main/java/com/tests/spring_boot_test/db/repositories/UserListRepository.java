package com.tests.spring_boot_test.db.repositories;

import com.tests.spring_boot_test.db.entities.UsersList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserListRepository extends JpaRepository<UsersList, Long> {
    UsersList findByUsername(String username);
}
