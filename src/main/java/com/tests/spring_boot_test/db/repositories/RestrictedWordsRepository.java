package com.tests.spring_boot_test.db.repositories;

import com.tests.spring_boot_test.db.entities.RestrictedWords;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RestrictedWordsRepository extends JpaRepository<RestrictedWords, Long> {
}
